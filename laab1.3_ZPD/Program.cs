﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace laab1._3_ZPD
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.OutputEncoding = Encoding.UTF8;

            char[] Message;
            string filepath = @"C:\Users\User\Desktop\Parts\test.txt";
            bool CryptSign = true;
            string type;
            string result = null;
            
            StreamReader streamReader = new StreamReader(filepath, System.Text.Encoding.UTF8);
            Message = streamReader.ReadToEnd().ToCharArray();
            streamReader.Close();

            Console.WriteLine("Оберіть дію (0- Зашифрувати, 1- Розшифрувати");
            Console.Write("->");
            type = Console.ReadLine();

            if (type == "0")
                CryptSign = true;
            else if (type == "1")
                CryptSign = false;

            result = Execute(CryptSign, Tostring(Message));
        
            StreamWriter streamWriter = new StreamWriter(filepath, false, Encoding.UTF8);
            streamWriter.Write(result);
            streamWriter.Close();

            Console.WriteLine(result);
        }


        static string Execute(bool CryptSign, string Message)
        {
            string[] Blocks;
            string Block;
            if (CryptSign)
            {
                Message = ToBin(Message.ToCharArray());
                Blocks = Divide_Into_Blocks(Message);
                Message = null;

                for (int i = 0; i < Blocks.Length; i++)
                {
                    Block = Blocks[i];
                    for (int j = 0; j < 3; j++)
                        Block = RoundEncrypt(Block);
                    Message += Block;
                }
            }

            if (!CryptSign)
            {
                Blocks = Divide_Into_Blocks(Message);
                Message = null;
                for (int i = Blocks.Length - 1; i >= 0; i--)
                {

                    Block = Blocks[i];
                    for (int j = 0; j < 9; j++)
                        Block = RoundEncrypt(Block);
                    Message += Block;
                }
                int[] ChCode = FromBin(Message.ToCharArray());

                Message = null;

                for (int i = 0; i < ChCode.Length; i++)
                    Message += Convert.ToChar(ChCode[i]).ToString();
            }

            return Message;

        }
        #region Виконання операції
        static string RoundEncrypt(string block)
        {

            string right = null;
            string left = null;
            string _right = null;
            string _left = null;

            Divide_Into_Subblocks(block, out left, out right);

            _left = right;
            right = KeyFunc(right);
            _right = XOR(left, right);
            return _right + _left;
        }
        static string RoundDecrypt(string block)
        {

            string right = null;
            string left = null;
            string _right = null;
            string _left = null;

            Divide_Into_Subblocks(block, out right, out left);

            _left = right;
            right = KeyFunc(right);
            _right = XOR(left, right);
            return _right + _left;
        }
        // Функція - Права частина інверсуеться, потім вона стає лівою частиною, а ліва - правою
        // Наприклад число 1001 1001 стає 0110 1001
        static string KeyFunc(string part)
        {
            string Right = null;
            string Left = null;

            Divide_Into_Subblocks(part, out Left, out Right);

            char[] temp = Right.ToCharArray();
            Right = null;

            for (int i = 0; i < temp.Length; i++)
            {
                if (temp[i] == '0')
                    Right += "1";
                else
                    Right += "0";
            }

            return Right + Left;
        }

        static string XOR (string opers1, string opers2)
        {
            char[] _opers1 = opers1.ToCharArray();
            char[] _opers2 = opers2.ToCharArray();
            string res = null;

            for(int i = 0; i< opers1.Length; i++)
            {
                if (_opers1[i] == _opers2[i])
                    res += "0";
                else
                    res += "1";
            }

            return res;
        }
        #endregion

        #region Операції з блоками (Розділення на блоки, підблоки)
        static string[] Divide_Into_Blocks(string code)
        {
            string[] blocks = new string[code.Length / 32]; // 32 - довжина блока
            char[] _code = code.ToCharArray();
            int n = 0;

            for (int i = 0; i < code.Length / 32; i++, n+=32)
 
                for (int j = n; j < n+32; j++)
                    blocks[i] += code[j].ToString();


            return blocks;
        }
        static void Divide_Into_Subblocks(string block, out string Left, out string Right)
        {
            Left = null;
            Right = null;
            char[] _block = block.ToCharArray();

            for(int i = 0; i< _block.Length; i++)
            {
                if (i < _block.Length / 2)
                    Left += _block[i].ToString();
                else
                    Right += _block[i].ToString();
            }
        }



        #endregion

        #region Переведення у бінарний код та з нього
        //Переведення у бінарний код
        static string ToBin(char[] str)
        {
            int[] nums = new int[str.Length];
            string code = null;

            for (int i = 0; i < str.Length; i++)
                nums[i] = Convert.ToInt32(str[i]);
            
            for (int i = 0; i < nums.Length; i++)
                code += SymbolToBin(nums[i]);

            //int[] res = new int[code.Length];

            //str = code.ToCharArray();

            //for (int i = 0; i < code.Length; i++)
            //{
            //    res[i] = Convert.ToInt32(str[i].ToString());
            //}

            return code;
        }
        static int[] FromBin(char[] str)
        {
            int[] res = new int[str.Length / 32];
            int tmp = 0;

            for (int i = 0, j = 0, z = 8; i < str.Length; i++, z--)
            {
                if (i % 32 == 0)
                {
                    res[j] = tmp;
                    j++;
                    tmp = 0;
                    z = 32;
                }
                tmp += Convert.ToInt32(str[i].ToString()) * 2 ^ z;

            }
            return res;
        }
        static string SymbolToBin(int s)
        {
            int tmp = s;
            string code = null;
            char[] temp;

            do
            {
                code += (tmp % 2).ToString();
                tmp /= 2;
            } while (tmp != 0);

            while (code.Length % 32 != 0)
                code += "0";
            temp = code.ToCharArray();
            Array.Reverse(temp);
            code = Tostring(temp);
            return code;
        }
        #endregion

        #region Додаткові методи
        static string Tostring(char[] arr)//Конвертація у массиву у рядок//DONE
        {
            string res = "";

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString();

            return res;
        }
        //static string Tostring(int[] arr)//Конвертація у массиву у рядок//DONE
        //{
        //    string res = "";

        //    for (int i = 0; i < arr.Length; i++)
        //        res += arr[i].ToString();

        //    return res;
        //}
        #endregion
    }

}
