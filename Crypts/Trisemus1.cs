﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Crypts
{
    class Trisemus1
    {
        protected char[] Message;
        protected char[] Alphabet;
        protected char CryptSign; // '+'- encrypt; '-' - decrypt;
        protected char[] Key;

        public Trisemus1 (string message, char sign, char[] alphabet, string key)
        {
            Message = message.ToCharArray();
            CryptSign = sign;
            Alphabet = alphabet;
            Key = key.ToCharArray();
        }

        public string Execute()
        {
            return execute();
        }

        protected string execute()
        {
            CryptTable table = new CryptTable(Alphabet, Key);
            char [] TrisemusTable = table.GetTrisemusTable();

            if (CryptSign == '+')
                return Encrypt(TrisemusTable);

            else
                return Decrypt(TrisemusTable);
        }

        
        protected string Encrypt(char [] TrisemusTable)
        {

            char[] result = new char [Message.Length];

            for (int i = 0; i < result.Length; i++) // передвижение по сообщению
                for (int j = 0; j < TrisemusTable.Length; j++) 
                {
                    if (Message[i] == TrisemusTable[j]) 
                    {
                        if (j >= TrisemusTable.Length - 6)
                            result[i] = TrisemusTable[j - TrisemusTable.Length + 6];
                        else
                        {
                                result[i] = TrisemusTable[j + 6];
                        }
                    }
                }

            return Tostring(result);
        }

        protected string Decrypt(char[] TrisemusTable)
        {
            char[] result = new char [Message.Length];

            for (int i = 0; i < result.Length; i++) // передвижение по сообщению
                for (int j = 0; j < TrisemusTable.Length; j++)
                {
                    

                    if (Message[i] == TrisemusTable[j])
                    {
                        if (j >= 6) 
                            result[i] = TrisemusTable[j - 6];

                        else // если символ - первая строка массива (переход в конец массива)
                        { 
                                result[i] = TrisemusTable[j + TrisemusTable.Length - 6];
                        }
                    }
                }

            return Tostring(result);
        }

        
        protected string Tostring(char[] arr)
        {
            string res = "";

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString();

            return res;
        }

    }
}
