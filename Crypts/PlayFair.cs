﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crypts
{
    class PlayFair
    {
        protected char[] Message;
        protected char[] Alphabet;
        protected char CryptSign; // '+'- encrypt; '-' - decrypt;
        protected char[] Key;
        public PlayFair(string message, char sign, char[] alphabet, string key)
        {
            Message = message.ToCharArray();
            CryptSign = sign;
            Alphabet = alphabet;
            Key = key.ToCharArray();
        }
        


        public string Execute()
        {
            return execute();
        }
        protected string execute()//виконання операції
        {
            CryptTable table = new CryptTable(Alphabet, Key);
            char[,] PlayFairTable = table.GetDoubleArray();

            char CryptSymbol;

            if (Alphabet[2] == 'C')
                CryptSymbol = 'X';
            else
                CryptSymbol = 'Я';


            if (CryptSign == '+')
                return Encrypt(PlayFairTable, CryptSymbol);

            else
                return Decrypt(PlayFairTable, CryptSymbol);
        }


        protected string Encrypt(char[,] PlayFairTable, char CryptSybmbol)//шифрування
        {
            char[] temp = CreateBigrams(CryptSybmbol).ToCharArray();
            int LeftJIndex = 0;
            int LeftZIndex = 0;
            int RightJIndex = 0;
            int RightZIndex = 0;
            string result = null;

            for (int i = 0; i < temp.Length; i += 2)
            {
                for (int j = 0; j < PlayFairTable.Length / 6; j++)
                    for (int z = 0; z < 6; z++)
                    {
                        if (temp[i] == PlayFairTable[j,z])
                        {
                            LeftJIndex = j;
                            LeftZIndex = z;
                        }
                        if (temp[i + 1] == PlayFairTable[j,z])
                        {
                            RightJIndex = j;
                            RightZIndex = z;
                        }
                    }
                // IF in one row
                if (LeftJIndex == RightJIndex)
                {
                    if (LeftZIndex == 5)
                        result += PlayFairTable[LeftJIndex,0];
                    else
                        result += PlayFairTable[LeftJIndex,LeftZIndex + 1];

                    if (RightZIndex == 5)
                        result += PlayFairTable[RightJIndex,0];
                    else
                        result += PlayFairTable[RightJIndex,RightZIndex + 1];
                }
                // if in one column
                else if (LeftZIndex == RightZIndex)
                {
                    if (LeftJIndex == PlayFairTable.Length / 6 - 1)
                        result += PlayFairTable[0,LeftZIndex];
                    else
                        result += PlayFairTable[LeftJIndex + 1,LeftZIndex];//

                    if (RightJIndex == PlayFairTable.Length / 6 - 1)
                        result += PlayFairTable[0,RightZIndex];
                    else
                        result += PlayFairTable[RightJIndex + 1,RightZIndex];
                }

                else
                {
                    result += PlayFairTable[LeftJIndex,RightZIndex];
                    result += PlayFairTable[RightJIndex,LeftZIndex];
                }
            }
            return result;
        }
        protected string Decrypt(char[,] PlayFairTable, char CryptSybmbol)//дешифрування
        {
            char[] temp = Message;
            int LeftJIndex = 0;
            int LeftZIndex = 0;
            int RightJIndex = 0;
            int RightZIndex = 0;
            string result = null;

            for (int i = 0; i < temp.Length; i += 2)
            {
                for (int j = 0; j < PlayFairTable.Length / 6; j++)
                    for (int z = 0; z < 6; z++)
                    {
                        if (temp[i] == PlayFairTable[j,z])
                        {
                            LeftJIndex = j;
                            LeftZIndex = z;
                        }
                        if (temp[i + 1] == PlayFairTable[j,z])
                        {
                            RightJIndex = j;
                            RightZIndex = z;
                        }
                    }
                // IF in one row
                if (LeftJIndex == RightJIndex)
                {
                    if (LeftZIndex == 0)
                        result += PlayFairTable[LeftJIndex,5];
                    else
                        result += PlayFairTable[LeftJIndex,LeftZIndex - 1];

                    if (RightZIndex == 0)
                        result += PlayFairTable[RightJIndex,5];
                    else
                        result += PlayFairTable[RightJIndex,RightZIndex - 1];
                }
                // if in one column
                else if (LeftZIndex == RightZIndex)
                {
                    if (LeftJIndex == 0)
                        result += PlayFairTable[PlayFairTable.Length / 6 - 1,LeftZIndex];
                    else
                        result += PlayFairTable[LeftJIndex - 1,LeftZIndex];

                    if (RightJIndex == 0)
                        result += PlayFairTable[PlayFairTable.Length / 6 - 1,RightZIndex];
                    else
                        result += PlayFairTable[RightJIndex - 1,RightZIndex];
                }

                else
                {
                    result += PlayFairTable[LeftJIndex,RightZIndex];
                    result += PlayFairTable[RightJIndex,LeftZIndex];
                }
            }

            temp = result.ToCharArray();

              if (temp.Length % 2 == 0 && temp[temp.Length-1] == CryptSybmbol)
                    temp = ArrayDeleteElement(temp, temp.Length-1);

            //Удаление вспомогательных сиволов
            for (int i = 0; i < temp.Length; i++)
                if (i != 0 && i != temp.Length - 1)
                {
                    if (temp[i - 1] == temp[i + 1] && temp[i] == CryptSybmbol)
                        temp = ArrayDeleteElement(temp, i);
                }

            result = Tostring(temp);
             return result;
        }
    

        protected string CreateBigrams(char CryptSymbol)//ствоерння біграм
        {
            char[] temp = Message;
            string result = Tostring(temp);

            for (int i = 0; i < temp.Length;i++)
            {
                if( i!=temp.Length - 1)
                if (temp[i] == temp[i + 1]) 
                    temp = ArrayInsert(CryptSymbol, temp, i + 1);
            }

            if (temp.Length %2 !=0)
                result = Tostring(temp) + CryptSymbol.ToString();
            else
                result = Tostring(temp);
            return result;
        }


        protected char[] ArrayInsert(char symbol, char[] arr, int index)// Вставлення миволу в масив
        {
            char[] result = new char[arr.Length + 1];
            int j = 0;

            for (int i = 0; i < result.Length; i++)
            {

                if (i == index)
                {
                    result[i] = symbol;
                    i++;
                }
                result[i] = arr[j];
                j++;
            }
            return result;
        }

        protected char[] ArrayDeleteElement(char[] arr, int index)// видалення символу з масиву
        {
            string result = null ;

            for(int i = 0; i<arr.Length;i++)
                if (i != index)
                    result += arr[i].ToString();
      
            return result.ToCharArray();
        }

        protected string Tostring(char[] arr)// конвертацыя масиву в рядок
        {
            string res = "";

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString();

            return res;
        }

    }
}
