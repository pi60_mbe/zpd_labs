﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crypts
{
    class Trisemus2
    {
        protected char[] Message;
        protected char[] Alphabet;
        protected char CryptSign; // '+'- encrypt; '-' - decrypt;

        public Trisemus2(string message, char sign, char[] alphabet)
        {
            Message = message.ToCharArray();
            CryptSign = sign;
            Alphabet = alphabet;
        }

        public string Execute()
        {
            return execute();
        }

        protected string execute()//виконання операції
        {
            CryptTable table = new CryptTable(Alphabet, null);
            char[][] TrisemusTable = table.GetTrisemus2Table();
            int n = Alphabet.Length;

            if (CryptSign == '+')
                return Encrypt(TrisemusTable, n);

            else
                return Decrypt(TrisemusTable, n);

        }

        protected string Encrypt(char[][] TrisemusTable, int nconst)//Шифрування
        {
            string result = null;
            char[] temp = Message;
            int n = nconst;

            for (int i = 0; i < temp.Length; i++)
            {
                for (int j = 0; j < nconst; j++)
                {

                        if (temp[i] == TrisemusTable[0][j])
                        {
                        if (i / n == 2)
                            n += Alphabet.Length;

                            
                            if (i < n)
                                result += TrisemusTable[i][j];
                            else
                                result += TrisemusTable[i - n][j];
                        }

                 }
            }

            return result;
        }

        protected string Decrypt(char[][] TrisemusTable, int nconst)//Дешиврування
        {
            string result = null;
            char[] temp = Message;
            int n = 0;

            for (int i = 0; i < temp.Length; i++)
            {
                if (i - n == nconst)
                    n += nconst;
                for (int z = 0; z < nconst; z++)
                        if (temp[i] == TrisemusTable[i-n][z])
                            result += TrisemusTable[0][z];
            }

                    

            return result;
        }


    }
}
