﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Crypts
{
    public class Start
    {
       
        protected string Message;
        protected string CryptMessage;


        public Start()
        {
            Menu();
        }

        protected void Menu()
        {
            Console.OutputEncoding = Encoding.UTF8;

            // Выделение английского и украинского алфавитов из ASCII. Верхние и нижние регистры
            char[] ukr = ("АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ .,").ToCharArray();
            char[] eng = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:").ToCharArray();
            short language;
            short choice;
            string filepath;
            char CryptSign;
            string key = null;
            string type;

        Console.Write("Шлях до файлу з текстом: ");
            filepath = Console.ReadLine();
            StreamReader streamReader = new StreamReader(filepath, System.Text.Encoding.Default);
            Message = streamReader.ReadToEnd();
            streamReader.Close();


        Error:
            Console.WriteLine("Оберіть дію (0- Зашифрувати, 1- Розшифрувати");
            Console.Write("->");
            type = Console.ReadLine();

            if (type == "0")
                CryptSign = '+';
            else if (type == "1")
                CryptSign = '-';
            else
            {
                Console.WriteLine("Не вірні дані. Спробуйте ще раз.");
                goto Error;
            }

            Console.WriteLine("ОБеріть мову тексту (0-Українська, 1-Англійська)");
            Console.Write("->");
            language = Convert.ToInt16(Console.ReadLine());


            Console.WriteLine("Оберіть тип шифрування (1- Трісемус1 2-PlayFair 3-Трісемус2)");
            Console.Write("->");
            choice = Convert.ToInt16(Console.ReadLine());

            if (choice != 3)
            {
                Console.Write("Введіть ключ:");
                key = Console.ReadLine();
            }

        Error1:
            switch (choice)
            {
                case 1:
                    Trisemus1 trisemus1 = null;
                    if (language == 0)
                        trisemus1 = new Trisemus1(Message, CryptSign, ukr, key);
                    else if (language == 1)
                        trisemus1 = new Trisemus1(Message, CryptSign, eng, key);

                    CryptMessage = trisemus1.Execute();

                    break;

                case 2:
                    PlayFair playfair = null;

                    if (language == 0)
                        playfair = new PlayFair(Message, CryptSign, ukr, key);
                    else if (language == 1)
                        playfair = new PlayFair(Message, CryptSign, eng, key);

                    CryptMessage = playfair.Execute();

                    break;

                case 3:
                    Trisemus2 trisemus2 = null;

                    if (language == 0)
                        trisemus2 = new Trisemus2(Message, CryptSign, ukr);
                    else if (language == 1)
                        trisemus2 = new Trisemus2(Message, CryptSign, eng);

                    CryptMessage = trisemus2.Execute();

                    break;


                default:
                    Console.WriteLine("Не вірні дані. Спробуйте ще раз.");
                    goto Error1;
            }
            StreamWriter streamWriter = new StreamWriter(filepath, false, Encoding.Default);
            streamWriter.Write(CryptMessage);
            streamWriter.Close();

            Console.WriteLine("Успешное выполнение операции");

        }


    }
}
