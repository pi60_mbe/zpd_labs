﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Crypts
{
    class CryptTable
    {
        protected char[] Alphabet;
        protected char[] Key;
        public CryptTable(char[] alphabet, char[] key)
        {
            Alphabet = alphabet;
            Key = key;
        }

        public char[] GetTrisemusTable()
        {
            return CreateCryptTable();
        }

        public char[,] GetDoubleArray()
        {
                return CreateDoubleArray();
        }
        
        public char[][] GetTrisemus2Table()
        {
            return Trisemus2Table();
        }


    protected char[] DeleteRepeatition(char[] arr)//видалення повторень з масиву
        {
            string table = Tostring(arr);
            table = new string(table.Distinct().ToArray());
            return table.ToCharArray();
        }

        protected char[] CreateCryptTable()//ствоерння таблиці шифрування для методу Трісемуса1
        {
            char[] temp;
            char[] result;
            int n = Key.Length + Alphabet.Length;
            temp = new char[n];
            int i = 0;
            
            for (int j = 0; j < temp.Length; j++)
            {

                if (j < Key.Length)
                    temp[j] = Key[j];

                else if (j >= Key.Length && j < n)
                {
                    temp[j] = Alphabet[i];
                    i++;
                }
            }
            // Убираем повторения символов

            temp = DeleteRepeatition(temp);

            n = temp.Length;// + (6 - temp.Length % 6);
            result = new char[n];
            //Дописываем 0 для полноценной таблицы
            for (int j = 0; j < n; j++)
            {
                if (j < temp.Length)
                    result[j] = temp[j];
                else
                    result[j] = '0';
            }

            return result;
        }

        protected char[,] CreateDoubleArray()//створення таблиці для шифрування Плейфером
        {
            char[] TempTable= GetTrisemusTable();
            int n = TempTable.Length / 6;
            char[,] PlayFairTable = new char[n,6];
            int TempIndex = 0;

            


            for (int i = 0; i < n; i++)
                for (int j = 0; j < TempTable.Length; j++)
                {
                    if (j == 6)
                        break;
                    else
                    {
                        PlayFairTable[i,j] = TempTable[TempIndex];
                        TempIndex++;
                    }
                }


            return PlayFairTable;
        }

        protected char[][] Trisemus2Table()//Для Трісемуса 2
        {
            char[][] result = new char[Alphabet.Length][];
            char[] temp = Alphabet;

            for (int i = 0; i < temp.Length; i++)
            {
                if (i != 0)
                    result[i] = ReWriteArray(temp, i);
                else
                    result[i] = temp;

            }
            return result;
        }

        protected char[] ReWriteArray(char[] arr, int index)//Зміщення алфавіту
        {
            string result = "";
            
            for (int i = index; result.Length != arr.Length; i++)
            {


                result += arr[i].ToString();
                if (i == arr.Length - 1)
                    i = -1;
                
            }

            return result.ToCharArray();
        }

        protected string Tostring(char[] arr)//Конвертація у массиву у рядок
        {
            string res = "";

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString();

            return res;
        }


    }
}