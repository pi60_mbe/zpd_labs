﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace lab_1._2
{
    class Transposition
    {
        private int[] key = null;

        public void SetKey(int[] _key)
        {
            key = new int[_key.Length];

            for (int i = 0; i < _key.Length; i++)
                key[i] = _key[i];
        }

        public void SetKey(string[] _key)
        {
            key = new int[_key.Length];

            for (int i = 0; i < _key.Length; i++)
                key[i] = Convert.ToInt32(_key[i]);
        }

        public void SetKey(string _key)
        {
            SetKey(_key.Split(' '));
        }

        public string Encrypt(string input)//DONE
        {
            for (int i = 0; i < input.Length % key.Length; i++)
                input += input[i];

            int n = input.Length / key.Length;
            
            string result = "";

            for (int i = 0; i < key.Length; i++)
            {
                char[] transposition = new char[n];

                for (int j = 0, z = 0; j < n; j++, z+=key.Length)
                    transposition[j] = input[key[i]-1 + z];

                for (int j = 0; j < n; j++)
                    result += transposition[j];
            }

            return result;
        }

        public string Decrypt(string input)//DONE
        {
            string result = "";
            for (int i = 0; i < input.Length % key.Length; i++)
                input += input[i];

            int n = input.Length / key.Length;
            int nj = n;

            for(int i = 0; i< n; i++, nj--)
            {
                char[] transposition = new char[key.Length];
                for (int j = input.Length - nj, z = 0; j >= 0; j -= n, z++)
                    transposition[z] = input[j];

                for (int j = 0; j < key.Length; j++)
                    result += transposition[j];
            }

            return result;
        }
    }
    public class CharNum
    {

        private char _ch;
        /// <summary>
        /// Порядковый номер зависящий от алфавита.
        /// </summary>
        private int _numberInWord;



        /// <summary>
        /// Символ.
        /// </summary>
        public char Ch
        {
            get { return _ch; }
            set
            {
                if (_ch == value)
                    return;
                _ch = value;
            }
        }
        /// <summary>
        /// Порядковый номер в строке, зависящий от алфавита.
        /// </summary>
        public int NumberInWord
        {
            get { return _numberInWord; }
            set
            {
                if (_numberInWord == value)
                    return;
                _numberInWord = value;
            }
        }

    }
    class Program
    {

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            char[] ukr = ("АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯ ").ToCharArray();
            char[] eng = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ ").ToCharArray();
            char[] Message;
            short language;
            short choice;
            string filepath = @"C:\Users\User\Desktop\Parts\test.txt";
            bool CryptSign;
            string key = null;
            string type;
            string result = null;

            //Console.Write("Шлях до файлу з текстом: ");
            //filepath = Console.ReadLine();
            StreamReader streamReader = new StreamReader(filepath, System.Text.Encoding.UTF8);
            Message = streamReader.ReadToEnd().ToCharArray();
            streamReader.Close();


        Error:
            Console.WriteLine("Оберіть дію (0- Зашифрувати, 1- Розшифрувати");
            Console.Write("->");
            type = Console.ReadLine();

            if (type == "0")
                CryptSign = true;
            else if (type == "1")
                CryptSign = false;
            else
            {
                Console.WriteLine("Не вірні дані. Спробуйте ще раз.");
                goto Error;
            }

            Console.WriteLine("ОБеріть мову тексту (0-Українська, 1-Англійська)");
            Console.Write("->");
            language = Convert.ToInt16(Console.ReadLine());


            Console.WriteLine("Оберіть тип шифрування (1- Вертикальна перестановка 2-Складна перестановка 3-Гамування по N 4- Гамування по 2)");
            Console.Write("->");
            choice = Convert.ToInt16(Console.ReadLine());

            //if (choice != 3)
            //{
                Console.Write("Введіть ключ:");
                key = Console.ReadLine();
            //}

        Error1:
            switch (choice)
            {
                case 1:

                    result = VerticalExecute(CryptSign, Tostring(Message), key.ToCharArray());
                    break;

                case 2:
                    if (language == 0)
                        result = EXECUTE(key, Tostring(Message), Tostring(ukr));
                    else
                        result = EXECUTE(key, Tostring(Message),Tostring(eng));

                    break;

                case 3:
                    if(language == 0)
                    result = GammaExecute(CryptSign, Message, key.ToCharArray(), ukr);
                    else
                        result = GammaExecute(CryptSign, Message, key.ToCharArray(), eng);

                    break;

                case 4:
                    if (language == 0)
                        result = Gamma2Execute(CryptSign, Message, key.ToCharArray(), ukr);
                    else
                        result = Gamma2Execute(CryptSign, Message, key.ToCharArray(), eng);

                    break;
                    


                default:
                    Console.WriteLine("Не вірні дані. Спробуйте ще раз.");
                    goto Error1;
            }
            StreamWriter streamWriter = new StreamWriter(filepath, false, Encoding.UTF8);
            streamWriter.Write(result);
            streamWriter.Close();

            Console.WriteLine(result);

        }

        public static string VerticalExecute(bool CryptSign, string Message, char[] Key)
        {
            string result = null;
            Transposition text = new Transposition();
            text.SetKey(NumericKey(Key));

            if (CryptSign)
                result = text.Encrypt(Message);
            else
                result = text.Decrypt(Message);
            return result;
            
        }
        static int[] NumericKey(char[] key)// нумерує символи ключа по порядку
        {
            string temp = Tostring(key);

            char[] _key = Sort(key);

            key = temp.ToCharArray();
            temp = null;

            int[] res = new int[key.Length];

            for (int i = 0; i < key.Length; i++)
                for (int j = 0; j < key.Length; j++)
                    if (key[i] == _key[j])
                    {
                        res[i] = j + 1;
                        _key[j] = '\0';
                        break;
                    }
            return res;
        }

        static string GammaExecute( bool CryptSign, char[] message, char[] key, char[] alphabet)// DONE
        {
            int[] gamma = CreateIndexArray(key, alphabet);

            int[] messagecode = CreateIndexArray(message, alphabet);

            int[] code = ModuleAdd(messagecode, gamma, message.Length);

            string res = null;

            if (CryptSign)
                for (int i = 0; i < code.Length; i++)
                    res += alphabet[code[i]];

            else
            {
                int temp;
                for (int i = 0; i < code.Length; i++)
                {
                    temp = messagecode[i] + message.Length - gamma[i];
                    res += alphabet[ temp%alphabet.Length];
                }
            }

            return res;
        }

        static string Gamma2Execute(bool CryptSign, char[] message, char[] key, char[] alphabet)
        {
            int[] gamma = ToBin(key,alphabet);
            string res = null;
            int[] code;

            if (CryptSign)
            {
                int[] messagecode = ToBin(message, alphabet);
                code = ModuleAdd(messagecode, gamma, 2);

                for (int i = 0; i < code.Length; i++)
                    res += code[i].ToString();

            }
            else
            { 
                for(int i = 0, j = 0; i< message.Length; i++, j++)
                {
                    if (j == gamma.Length)
                        j = 0;
                    res += GetDecryptSymbol(Convert.ToInt32(message[i].ToString()), gamma[j]).ToString();
                }

                code = FromBin(res.ToCharArray(), alphabet);
                res = null;

                for (int i = 0; i < code.Length; i++)
                    res += alphabet[code[i]];
                
               
            }

            return res;

        }

        static int[] ToBin(char[] str, char[] alpha )
        {
            int[] nums = new int[str.Length];
            string code = null;

            for (int i = 0; i < str.Length; i++)
                for (int j = 0; j < alpha.Length; j++)
                    if (str[i] == alpha[j])
                    { 
                      nums[i] = j;
                      break;
                    }
            for (int i = 0; i < nums.Length; i++)
                code += SymbolToBin(nums[i]);

            int[] res = new int[code.Length];

            str = code.ToCharArray();

            for(int i = 0; i< code.Length; i++)
            {
                res[i] = Convert.ToInt32(str[i].ToString());
            }

            return res;
        }

        static int[] FromBin(char[] str, char[] alpha)
        {
            int[] res = new int[str.Length / 8];
            int tmp = 0;

            for (int i = 0, j = 0, z = 8; i < str.Length; i++, z--) 
            {
                if (i % 8 == 0)
                {
                    res[j] = tmp;
                    j++;
                    tmp = 0;
                    z = 8;
                }

                tmp += Convert.ToInt32(str[i].ToString()) * 2 ^ z;
            }
            return res;
        }

        static string SymbolToBin(int s)
        {
            int tmp = s;
            string code = null;
            char[] temp;

            do
            {
                code += (tmp % 2).ToString();
                tmp /= 2;
            } while (tmp != 0);

                while (code.Length % 32 != 0)
                code += "0";
            temp = code.ToCharArray();
            Array.Reverse(temp);
            code = Tostring(temp);
            return code;

        }
    


            static int[] CreateIndexArray(char[] arr, char[] alphabet)//DONE
            {
            int[] res = new int[arr.Length];

            for (int i = 0; i < arr.Length; i++)
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (arr[i] == alphabet[j])
                    {
                        res[i] = j;
                    }
                }
            return res;

            }

        static int[] ModuleAdd(int[] oper1, int[] oper2, int module)// DONE
        {
            int[] res = new int[oper1.Length];
            int gammaindex = 0;
            if (module != 2)
            {
                for (int i = 0; i < oper1.Length; i++, gammaindex++)
                {
                    if (gammaindex == oper2.Length)
                        gammaindex = 0;
                    res[i] = oper1[i] + oper2[gammaindex];
                
                 if (res[i] > module)
                        res[i] %= module;

                    else
                        continue;
                }
            }
            else
            {
                for (int i = 0; i < oper1.Length; i++, gammaindex++)
                {
                    if (gammaindex == oper2.Length)
                        gammaindex = 0;
                    if (oper1[i] == oper2[gammaindex])
                        res[i] = 0;
                    else
                        res[i] = 1;
                }
            }



            return res;
        }

        static int GetDecryptSymbol(int message, int gamma )
        {
            int res;

            if (message == 0)
                res = gamma;
            else
            {
                if (gamma == 1)
                    res = 0;
                else
                    res = 1;
            }
            return res;
        }
    

    static char[] Sort(char [] arr)//DONE
        {
            Array.Sort(arr);
                return arr;
        }
        
        static string Tostring(char[] arr)//Конвертація у массиву у рядок//DONE
        {
            string res = "";

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString();

            return res;
        }
        /////////////////////////////////////////
        ///
        public static int GetNumberInThealphabet(char s, string alpha)
        {

            int number = alpha.IndexOf(s) / 2;

            return number;
        }

        
        public static List<CharNum> FillListKey(char[] chars, string alphabet)
        {
            List<CharNum> listKey = new List<CharNum>(chars.Length);

            for (int i = 0; i < chars.Length; i++)
            {
                CharNum charNum = new CharNum()
                {
                    Ch = chars[i],
                    NumberInWord = GetNumberInThealphabet(chars[i], alphabet)
                };

                listKey.Add(charNum);
            }
            return listKey;
        }
        /// <summary>
        /// Отображение ключа.
        /// </summary>
  
        public static void ShowKey(List<CharNum> listCharNum, string message)
        {
            Console.WriteLine(message);

            foreach (var i in listCharNum)
            {
                Console.Write(i.Ch + " ");
            }
            Console.WriteLine();

            foreach (var i in listCharNum)
            {
                Console.Write(i.NumberInWord + " ");
            }
            Console.WriteLine();
            Console.WriteLine();
        }
       
        public static List<CharNum> FillingSerialsNumber(
            List<CharNum> listCharNum)
        {
            int count = 0;

            var result = listCharNum.OrderBy(a =>
                a.NumberInWord);

            foreach (var i in result)
            {
                i.NumberInWord = count++;
            }

            return listCharNum;
        }

        public static string EXECUTE(string key, string message, string alphabet)
        {
            string firstKey = key;
            // Второй ключ, количество строк
            string secondKey = key;
            // Предложение которое шифруем
            string stringUser = message;

            // Матрица в которой производим шифрование
            char[,] matrix = new char[secondKey.Length, firstKey.Length];

            // Счетчик символов в строке
            int countSymbols = 0;

            // Переводим строки в массивы типа char
            char[] charsFirstKey = firstKey.ToCharArray();
            char[] charsSecondKey = secondKey.ToCharArray();
            char[] charStringUser = stringUser.ToCharArray();
            string CharStringUser = Tostring(charStringUser);

            for(int i = 0; i< charsFirstKey.Length * charsSecondKey.Length - 1; i++)
            {
                CharStringUser += " ";
            }

            charStringUser = CharStringUser.ToCharArray();

            // Создаем списки в которых будут храниться символы и порядковы номера символов
            List<CharNum> listCharNumFirst =
                new List<CharNum>(firstKey.Length);

            List<CharNum> listCharNumSecond =
                new List<CharNum>(secondKey.Length);

            // Заполняем символами из ключей
            listCharNumFirst = FillListKey(charsFirstKey, alphabet);
            listCharNumSecond = FillListKey(charsSecondKey, alphabet);

            // Заполняем порядковыми номерами
            listCharNumFirst = FillingSerialsNumber(listCharNumFirst);
            listCharNumSecond = FillingSerialsNumber(listCharNumSecond);

            ShowKey(listCharNumFirst, "Первый ключ: ");
            ShowKey(listCharNumSecond, "Второй ключ: ");


            // Заполнение матрицы строкой пользователя
            for (int i = 0; i < listCharNumSecond.Count; i++)
            {
                for (int j = 0; j < listCharNumFirst.Count; j++)
                {
                    if(countSymbols >= charStringUser.Length)
                    {
                        matrix[i, j] = ' ';
                        break;
                    }
                    matrix[i, j] = charStringUser[countSymbols++];
                }
            }

          

            countSymbols = 0;
            // Заполнение матрицы с учетом шифрования. 
            // Переставляем столбцы по порядку следования в первом ключе. 
            // Затем переставляем строки по порядку следования во втором ключа. 
            for (int i = 0; i < listCharNumSecond.Count; i++)
            {
                for (int j = 0; j < listCharNumFirst.Count; j++)
                {
                    matrix[listCharNumSecond[i].NumberInWord,
                       listCharNumFirst[j].NumberInWord] = charStringUser[countSymbols++];
                }
            }

            string res = null;
            for (int i = 0; i < secondKey.Length; i++)
                for (int j = 0; j < firstKey.Length; j++)
                    res += matrix[i, j];
            return res;
            
        }
    }
}
