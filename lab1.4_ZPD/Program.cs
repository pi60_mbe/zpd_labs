﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace lab1._4_ZPD
{
    class Program
    {
        static void Main(string[] args)
        {
            string filepathsource = @"C:\Users\User\Desktop\Parts\test.txt";
            string filepathdestination = @"C:\Users\User\Desktop\Parts\res.txt";
            string type;
            string key;
    
            Console.WriteLine("Ключ(8 символів): ");
            Console.Write("->");
            key = Console.ReadLine();

            Console.WriteLine("Оберіть дію (0- Зашифрувати, 1- Розшифрувати");
            Console.Write("->");
            type = Console.ReadLine();

            if (type == "0")
                Encrypt( key);
            else if (type == "1")
                Decrypt( key);

        }

        static void Encrypt( string key)
        {
            string filepathsource = @"C:\Users\User\Desktop\Parts\test.txt";
            string filepathdestination = @"C:\Users\User\Desktop\Parts\res.des";
            FileStream message = new FileStream(filepathsource,FileMode.Open, FileAccess.Read);
            FileStream encrypted = new FileStream(filepathdestination,FileMode.Open,FileAccess.Write);


            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();

            DES.Key = ASCIIEncoding.ASCII.GetBytes(key);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(key); 

            ICryptoTransform crypt = DES.CreateEncryptor();
            CryptoStream cryptoStream = new CryptoStream(encrypted, crypt, CryptoStreamMode.Write);

            byte[] byteMessage = new byte[message.Length - 0];
            message.Read(byteMessage, 0, byteMessage.Length);
            cryptoStream.Write(byteMessage, 0, byteMessage.Length);

            cryptoStream.Close();
            message.Close();
            encrypted.Close();


        }
        static void Decrypt( string key)
        {
            string filepathsource = @"C:\Users\User\Desktop\Parts\res.des";
            string filepathdestination = @"C:\Users\User\Desktop\Parts\test.txt";
            FileStream message = new FileStream(filepathsource, FileMode.Open, FileAccess.Read);
            FileStream encrypted = new FileStream(filepathdestination, FileMode.Open, FileAccess.Write);

            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();

            DES.Key = ASCIIEncoding.ASCII.GetBytes(key);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(key);

            ICryptoTransform crypt = DES.CreateDecryptor();
            CryptoStream cryptoStream = new CryptoStream(encrypted, crypt, CryptoStreamMode.Write);

            byte[] byteMessage = new byte[message.Length - 0];
            message.Read(byteMessage, 0, byteMessage.Length);
            cryptoStream.Write(byteMessage, 0, byteMessage.Length);

            cryptoStream.Close();
            message.Close();
            encrypted.Close();
        }


    }
}
